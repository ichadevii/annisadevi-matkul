from django import forms
from .models import MataKuliah

class Input_Form(forms.ModelForm):
        class Meta:
            model = MataKuliah
            fields = ['namamatkul', 'dosen', 'sks', 'deskripsi','semester', 'ruang']

        error_messages={
            'required' : 'Please Type'
        }

        matkul_attrs = {
            'type' : 'text',
            'class' : 'form-control',
            'placeholder' : 'PPW'
        }

        dosen_attrs = {
            'type' : 'text',
            'class' : 'form-control',
            'placeholder' : 'Bu Iis'
        }

        sks_attrs = {
            'type' : 'text',
            'class' : 'form-control',
            'placeholder' : '3 sks'
        }

        deskripsi_attrs = {
            'type' : 'text',
            'class' : 'form-control',
            'placeholder' : 'PPW mempelajari website'
        }

        semester_attrs = {
            'type' : 'text',
            'class' : 'form-control',
            'placeholder' : 'Gasal 2020/2021'
        }

        ruang_attrs = {
            'type' : 'text',
            'class' : 'form-control',
            'placeholder' : 'R.2016'
        }


        namamatkul = forms.CharField(label='Nama Mata Kuliah', required=True, max_length=30, widget=forms.TextInput(attrs=matkul_attrs))
        dosen = forms.CharField(label='Dosen Pengajar', required=True, max_length=30, widget=forms.TextInput(attrs=dosen_attrs))
        sks = forms.CharField(label='Jumlah SKS', required=True, max_length=10, widget=forms.TextInput(attrs=sks_attrs))
        deskripsi = forms.CharField(label='Deskripsi', required=True, max_length=70, widget=forms.TextInput(attrs=deskripsi_attrs))
        semester = forms.CharField(label='Semester Tahun', required=True, max_length=20, widget=forms.TextInput(attrs=semester_attrs))
        ruang = forms.CharField(label='Ruang Kelas', required=True, max_length=10, widget=forms.TextInput(attrs=ruang_attrs))