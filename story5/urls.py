from django.urls import path

from . import views

app_name = 'story5'

urlpatterns = [
    path('simpanmatkul', views.simpanmatkul, name='simpanmatkul'),
    path('bacamatkul', views.bacamatkul, name='bacamatkul'),
    path('detailmatkul/<str:id_det>', views.detailmatkul, name='detailmatkul'),
    path('cekhapusmatkul', views.hapusmatkul, name='hapusmatkul'),
    path('hapusmatkul/<str:id_det>', views.hapusmatkul, name='hapusmatkul'),
]
