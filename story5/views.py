from django.shortcuts import render, redirect
from django.http import HttpResponseRedirect
from .forms import Input_Form
from .models import MataKuliah

# Create your views here.
response = {}
def simpanmatkul(request):
    form = Input_Form(request.POST or None)
    if request.method=="POST":
        if form.is_valid:
            form.save()
            return HttpResponseRedirect('bacamatkul')
    else:
        form = Input_Form()
        response['form'] = form

        matkul=MataKuliah.objects.all()
        response['matkul'] = matkul

        return render(request, 'story5/matkul.html', response)

def bacamatkul(request):
    return HttpResponseRedirect('simpanmatkul')

def detailmatkul(request, id_det):
    details = MataKuliah.objects.get(id=id_det)
    response['matkultujuan'] = details
    return render (request, 'story5/detailmatkul.html', response)

def hapusmatkul(request, id_det):
    details = MataKuliah.objects.get(id=id_det)
    details.delete()
    return HttpResponseRedirect('/simpanmatkul')
