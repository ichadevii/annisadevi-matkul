from django.db import models

# Create your models here.
class MataKuliah(models.Model):
    namamatkul = models.CharField(max_length=30)
    dosen = models.CharField(max_length=30)
    sks = models.CharField(max_length=10)
    deskripsi = models.CharField(max_length=70)
    semester = models.CharField(max_length=20)
    ruang = models.CharField(max_length=10)