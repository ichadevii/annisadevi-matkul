from django.urls import path

from . import views

app_name = 'story7'

urlpatterns = [
    path('kegiatan/', views.kegiatan, name='kegiatan'),
]