from django.test import TestCase

# Create your tests here.
from django.test import LiveServerTestCase, TestCase, tag, Client
from django.urls import reverse, resolve
from selenium import webdriver
from django.http import HttpRequest
from .views import kegiatan

@tag('functional')
class FunctionalTestCase(LiveServerTestCase):
    """Base class for functional test cases with selenium."""

    @classmethod
    def setUpClass(cls):
        super().setUpClass()
        # Change to another webdriver if desired (and update CI accordingly).
        options = webdriver.chrome.options.Options()
        # These options are needed for CI with Chromium.
        options.headless = True  # Disable GUI.
        options.add_argument('--no-sandbox')
        options.add_argument('--disable-dev-shm-usage')
        cls.selenium = webdriver.Chrome(options=options)

    @classmethod
    def tearDownClass(cls):
        cls.selenium.quit()
        super().tearDownClass()


class KegiatanUnitTestURL(TestCase):

    # Cek URL kegiatan (Halaman Utama)
    def test_apakah_ada_url_kegiatan(self):
        response = Client().get('/kegiatan/')
        self.assertEqual(response.status_code, 200)

    # Cek apakah path / fungsinya adalah kegiatan dan templatenya kegiatan.html
    def test_apakah_view_dan_template_dari_path_kegiatan(self):
        response = self.client.get('/kegiatan/')
        found = resolve('/kegiatan/')
        self.assertEqual(found.view_name, "story7:kegiatan")
        self.assertTemplateUsed(response, 'story7/kegiatan.html')
        
# class KegiatanUnitTestHTML(TestCase):
    #Cek Isi HTML Kegiatan yang ada di views kegiatan + apakah method get pada views kegiatan menampilkan form kosong
    def test_isi_html_kegiatan(self):
        request = HttpRequest()
        response = kegiatan(request)
        html_kegiatan = response.content.decode('utf 8')
        self.assertIn("Organisasi/Kepanitiaan", html_kegiatan)
        self.assertIn("Prestasi", html_kegiatan)
        self.assertIn("Aktivitas Saat Ini", html_kegiatan)
        self.assertIn("Hobi", html_kegiatan)