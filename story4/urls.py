from django.urls import path

from . import views

app_name = 'story4'

urlpatterns = [
    path('profile2', views.profile2, name='profile2'),
    path('hobby', views.hobby, name='hobby'),
]
