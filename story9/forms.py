from django import forms
from django.contrib.auth.forms import UserCreationForm
from django.contrib.auth.models import User

class MyUserCreationForm(UserCreationForm):
    class Meta:
        model = User
        fields = ['username', 'email', 'first_name', 'last_name', 'password1', 'password2']

    error_messages = {
        'required' : 'Please Type',
        'password_mismatch' : "Password and confirm password don't match"
    }

    username_attrs = {
        'placeholder' : 'Username',
        'class' : 'form-control',
        'required' : True,
        'unique' : True
    }

    email_attrs = {
        'placeholder' : 'Email',
        'class' : 'form-control',
        'required' : True,
    }

    first_name_attrs = {
        'placeholder' : 'First Name',
        'class' : 'form-control',
        'required' : True,
    }

    last_name_attrs = {
        'placeholder' : 'Last Name',
        'class' : 'form-control',
        'required' : True,
    }

    password1_attrs = {
        'placeholder' : 'Password',
        'class' : 'form-control',
        'required' : True,
    }

    password2_attrs = {
        'placeholder' : 'Confirm Password',
        'class' : 'form-control',
        'required' : True,
    }

    username = forms.SlugField(label="Username",widget=forms.TextInput(attrs=username_attrs))
    email = forms.EmailField(label="Email",widget=forms.EmailInput(attrs=email_attrs))
    first_name = forms.CharField(label="First Name",widget=forms.TextInput(attrs=first_name_attrs))
    last_name = forms.CharField(label="Last Name",widget=forms.TextInput(attrs=last_name_attrs))
    password1 = forms.CharField(label="Password",widget=forms.PasswordInput(attrs=password1_attrs))
    password2 = forms.CharField(label="Confirm Password",widget=forms.PasswordInput(attrs=password2_attrs))

class MyLoginForm(forms.Form):

    error_messages = {
        'required' : 'Please Type',
        'unique': 'About user with this username already exists.',
        'password_mismatch' : "Password and confirm password don't match"
    }

    username_attrs = {
        'placeholder' : 'Username',
        'class' : 'form-control',
        'required' : True,
        'unique' : True
    }

    password1_attrs = {
        'placeholder' : 'Password',
        'class' : 'form-control',
        'required' : True,
    }

    username = forms.SlugField(widget=forms.TextInput(attrs=username_attrs))
    password1 = forms.CharField(widget=forms.PasswordInput(attrs=password1_attrs))