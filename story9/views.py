from django.shortcuts import render

# Create your views here.
from django.shortcuts import render, redirect
from .forms import MyUserCreationForm, MyLoginForm
from django.contrib.auth.models import User
from django.contrib import messages
from django.contrib.auth.decorators import login_required
from django.contrib.auth import login,authenticate,logout

# Create your views here.

response = {}
def register(request):
    form = MyUserCreationForm(request.POST)
    if request.method=='POST' :
        if form.is_valid():
            form.save()
            name = form.cleaned_data.get('username')
            messages.success(request, "Selamat " + name +" berhasil membuat akun, silahkan login.")
            return redirect ("/loginAkun/")
        else:
            messages.warning(request, form.errors)
            return redirect ("/register/")
    form = MyUserCreationForm()
    response['form'] = form
    return render(request, 'story9/register.html', response)

def loginAkun(request):
    form = MyLoginForm(request.POST)
    if request.method=='POST':
        if form.is_valid():
            username = form.cleaned_data.get('username')
            password = form.cleaned_data.get('password1')
        
            if username=="" or password=="":
                messages.warning(request, form.errors)
            else:
                user = authenticate(username=username, password=password)
                if user : 
                    login(request, user)
                    return redirect("/ajax/")
                else:
                    messages.warning(request, "Invalid username or password")
                    return redirect ("/loginAkun/")
    
    form = MyLoginForm()
    response['form'] = form
    return render(request, 'story9/login.html', response)

@login_required
def logoutAkun(request):
    logout(request)
    return redirect("/ajax/")