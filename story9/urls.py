from django.urls import path

from . import views

app_name = 'story9'

urlpatterns = [
    path('loginAkun/', views.loginAkun, name='loginAkun'),
    path('logoutAkun/', views.logoutAkun, name='logoutAkun'),
    path('register/', views.register, name='register'),
    
]