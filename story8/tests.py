from django.test import TestCase

# Create your tests here.
from django.test import LiveServerTestCase, TestCase, tag, Client
from django.urls import reverse, resolve
from selenium import webdriver
from django.http import HttpRequest, JsonResponse
from .views import ajax, data
import requests
import json

@tag('functional')
class FunctionalTestCase(LiveServerTestCase):
    """Base class for functional test cases with selenium."""

    @classmethod
    def setUpClass(cls):
        super().setUpClass()
        # Change to another webdriver if desired (and update CI accordingly).
        options = webdriver.chrome.options.Options()
        # These options are needed for CI with Chromium.
        options.headless = True  # Disable GUI.
        options.add_argument('--no-sandbox')
        options.add_argument('--disable-dev-shm-usage')
        cls.selenium = webdriver.Chrome(options=options)

    @classmethod
    def tearDownClass(cls):
        cls.selenium.quit()
        super().tearDownClass()


class AjaxUnitTestURL(TestCase):

    # Cek URL ajax
    def test_apakah_ada_url_ajax(self):
        response = Client().get('/ajax/')
        self.assertEqual(response.status_code, 200)

    # Cek apakah path / fungsinya adalah ajax dan templatenya ajax.html
    def test_apakah_view_dan_template_dari_path_ajax(self):
        response = self.client.get('/ajax/')
        found = resolve('/ajax/')
        self.assertEqual(found.view_name, "story8:ajax")
        self.assertTemplateUsed(response, 'story8/ajax.html')
    
    # Cek URL data
    def test_apakah_ada_url_data(self):
        response = Client().get('/data?q=')
        self.assertEqual(response.status_code, 301)

    # def test_view_data(self):
    #     request = self.client.get("/data/?q=" + "website")
    #     url = "https://www.googleapis.com/books/v1/volumes?q=" + "website" 
    #     hasil = requests.get(url)
    #     data = json.loads(hasil.content)
    #     data1= json.loads(request.content.decode())
    #     array_item = data['items']
    #     i = 0
    #     for x in array_item:
    #         del array_item[i]['volumeInfo']['imageLinks']['smallThumbnail']
    #         i = i+1
    #     self.assertEqual(data1, data)
