$(document).ready(function(){
        $.ajax({
            url : "/data?q=bts",
            type : "GET",
            success : function(request){
                var arr = request.items;
            
                for (i=0; i<arr.length; i++){
                    var title = arr[i].volumeInfo.title;
                    var penulis = arr[i].volumeInfo.authors;
                    var thumbnail = arr[i].volumeInfo.imageLinks.smallThumbnail;
                    var linkInfo = arr[i].accessInfo.webReaderLink;
                    var number = i+1;
                    $("#listBuku").append("<tr> <th scope='row'>" + number + "</th> <td>" + title + "</td> <td>" + penulis + "</td> <td> <a id='linkInfo' href='" + linkInfo + "'>" + linkInfo + "</a></td> <td><img src=" + thumbnail + "> </td> </tr>");
                    
                }
                console.log(request);
                $("#search").val("bts");
            }
        });

    // Ketika dilepas dari keyboard, saat nulis di form search
    $("#search").keyup(function(){
        // letter berisi string yang ada di value tag input
        var letter = $('#search').val(); 
        console.log(letter);

        if (letter===""){
            letter = "bts";
        }

        $.ajax({
            url : "/data?q=" + letter,
            type : "GET",
            success : function(data){
                var arr_items = data.items;
                // console.log(arr_items);
                $("#listBuku").empty();
                for (item=0; item<arr_items.length; item++){
                    var title = arr_items[item].volumeInfo.title;
                    var penulis = arr_items[item].volumeInfo.authors;
                    var thumbnail = arr_items[item].volumeInfo.imageLinks.smallThumbnail;
                    var linkInfo = arr_items[item].accessInfo.webReaderLink;
                    var number = item+1;
                    $("#listBuku").append("<tr> <th scope='row'>" + number + "</th> <td>" + title + "</td> <td>" + penulis + "</td> <td> <a id='linkInfo' href='" + linkInfo + "'>" + linkInfo + "</a></td> <td><img src=" + thumbnail + "> </td> </tr>");
                    console.log(arr_items);
                }
            }
    
        });
    });
});