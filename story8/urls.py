from django.urls import path

from . import views

app_name = 'story8'

urlpatterns = [
    path('ajax/', views.ajax, name='ajax'),
    path('data/', views.data, name='data'),
]