from django.shortcuts import render, redirect
from django.http import JsonResponse
import requests
import json

# Create your views here.
def ajax(request):
    return render(request, 'story8/ajax.html')

def data(request):
    url = "https://www.googleapis.com/books/v1/volumes?q=" + request.GET["q"]
    hasil = requests.get(url)
    data = json.loads(hasil.content)

    if not request.user.is_authenticated :
        array_item = data['items']
        i = 0
        for x in array_item:
            del array_item[i]['volumeInfo']['imageLinks']['smallThumbnail']
            i = i+1

    return JsonResponse(data, safe=False)
# safe = False -> data bukan dict