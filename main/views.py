from django.shortcuts import render


def profile(request):
    return render(request, 'main/profile.html')